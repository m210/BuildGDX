package ru.m210projects.Build.input;

public interface GameController {

    String getAxisName(int axisCode);
    String getButtonName(int buttonCode);

}
